;-----------------------------------------------
;       CONSTANTS
;----------------------------------------------
JOY1                    =$DC00
JOY2                    =$DC01

KEYCODE_SPACE           = 60
KEYCODE_2               = 59
KEYCODE_3               = 8
KEYCODE_4               = 11
KEYCODE_i               = 33
KEYCODE_s               = 13
KEYCODE_c               = 20

COLOR_BLACK           = 0
COLOR_WHITE           = 1
COLOR_RED             = 2
COLOR_CYAN            = 3
COLOR_PURPLE          = 4
COLOR_GREEN           = 5
COLOR_BLUE            = 6
COLOR_YELLOW          = 7
COLOR_ORANGE          = 8
COLOR_BROWN           = 9
COLOR_LIGHT_RED       = 10
COLOR_DARK_GREY       = 11
COLOR_MED_GREY        = 12
COLOR_LIGHT_GREEN     = 13
COLOR_LIGHT_BLUE      = 14
COLOR_LIGHT_GREY      = 15

BORDER_COLOR            =$D020
BACKGR_COLOR_0          = $D021
BACKGR_COLOR_1          = $D022
BACKGR_COLOR_2          = $D023
BACKGR_COLOR_3          = $D024

SPRITE_MULTICOLOR_1     = $D025
SPRITE_MULTICOLOR_2     = $D026

VICSTART                = $0000

SPRITE_POINTER1                =VICSTART+2040
SPRITE_POINTER2                =VICSTART+2041
SPRITE_POINTER3                =VICSTART+2042
SPRITE_POINTER4                =VICSTART+2043
SPRITE_POINTER5                =VICSTART+2044
SPRITE_POINTER6                =VICSTART+2045
SPRITE_POINTER7                =VICSTART+2046
SPRITE_POINTER8                =VICSTART+2047

SPRITE_ENABLE   = $D015


MULTICOLOR1             = $D025
MULTICOLOR2             = $D026

SPRITE_X_MSB            = $D010

RANDOM                  = $D41B

;--------- SID----------------------
SID_BASE                = $D400
FILTER_REGISTER_LO      = $D415
FILTER_REGISTER_HI      = $D416                


*= $0801

GenerateTo   "catscratch.prg"

;--------------------------------------------------
;       BASIC STARTER
;--------------------------------------------------

        BYTE $0C, $08  ; pointer to next line
        BYTE $0A, $00  ; line number (10)
        BYTE $9E       ; SYS token

        TEXT " 2062" ; SYS address in ASCII
        
        BYTE 0, 0, 0 ; end-of-program

        ;;bytes 0 * 55
;--------------------------------------------------
;       start programma
;--------------------------------------------------
                jsr setup_sprites
                jsr copy_sid_wiz
                jsr setup_music
                jsr setup_irq
                jsr copy_sprites
                
                rts


setup_music     lda #1
                jsr $c000
                rts

;copy sidd wizard
copy_sid_wiz
        ldx #255
@loop   lda     sidwiz,x
        sta     $c000,x
        lda     sidwiz+256,x
        sta     $c000+256,x
        lda     sidwiz+512,x
        sta     $c000+512,x
        lda     sidwiz+768,x
        sta     $c000+768,x
        lda     sidwiz+1024,x
        sta     $c000+1024,x
        lda     sidwiz+1280,x
        sta     $c000+1280,x
        lda     sidwiz+1536,x
        sta     $c000+1536,x
        lda     sidwiz+1792,x
        sta     $c000+1792,x
        lda     sidwiz+2048,x
        sta     $c000+2048,x
        lda     sidwiz+2304,x
        sta     $c000+2304,x
        lda     sidwiz+2560,x
        sta     $c000+2560,x
        lda     sidwiz+2816,x
        sta     $c000+2816,x
        lda     sidwiz+3072,x
        sta     $c000+3072,x
        lda     sidwiz+3328,x
        sta     $c000+3328,x
        lda     sidwiz+3584,x
        sta     $c000+3584,x
        dex        
        cpx #255
        bne @loop
        rts


copy_sprites_error    ldx #255
@loop           lda     start_sprite_data,x
                sta     $3000,x
                lda     start_sprite_data+256,x
                sta     $3000+256,x
                lda     start_sprite_data+512,x
                sta     $3000+512,x
                lda     start_sprite_data+768,x
                sta     $3000+768,x
                lda     start_sprite_data+1024,x
                sta     $3000+1024,x
                lda     start_sprite_data+1280,x
                sta     $3000+1280,x
                lda     start_sprite_data+1536,x
                sta     $3000+1536,x
                lda     start_sprite_data+1792,x
                sta     $3000+1792,x
                lda     start_sprite_data+2048,x
                sta     $3000+2048,x
                lda     start_sprite_data+2304,x
                sta     $3000+2304,x
                lda     start_sprite_data+2560,x
                sta     $3000+2560,x
                lda     start_sprite_data+2816,x
                sta     $3000+2816,x
                lda     start_sprite_data+3072,x
                sta     $3000+3072,x
                lda     start_sprite_data+3328,x
                sta     $3000+3328,x
                lda     start_sprite_data+3584,x
                sta     $3000+3584,x
                dex        
                cpx #255
                bne @loop
                rts
copy_sprites
                lda #<start_sprite_data
                sta cs2+1
                lda #>start_sprite_data
                sta cs2+2
                lda #$00
                sta cs3+1
                lda #$30
                sta cs3+2
                ldy #16
cs1             ldx #0
cs2             lda $FFFF,x
cs3             sta $3000,x
                inx
                bne cs2
                inc cs2+2
                inc cs3+2
                dey
                ;cpy #16
                bne cs1
                rts
                        

setup_sprites   ldy #160
                ldx #0
                lda #30
@loop1          sta $d000,x
                inx
                inx
                clc
                adc #50
                cpx #12
                bne @loop1
                
                lda #49
                sta $d000+12

                ldx #0
                lda #160
@loop2          sta $d001,x
                inx
                inx
                cpx #12
                bne @loop2
                lda #160-21
                sta $d001+4
                sta $d001+6
                sta $d001+10
                lda #160-18
                sta $d001+12

                ldx #0
                lda #192
@loop3          sta SPRITE_POINTER1,x
                inx
                clc     
                adc #3
                cpx #8
                bne @loop3

                lda #255
                sta SPRITE_ENABLE
                sta 53276       ; multicolor        
                lda #COLOR_BLACK
                sta SPRITE_MULTICOLOR_2
                lda #COLOR_MED_GREY
                sta SPRITE_MULTICOLOR_1
                lda #COLOR_WHITE        
                sta 53287       ; mouse 1
                sta 53288       ; mouse 2
                sta 53289       ; cat 1
                sta 53290       ; cat 2
                sta 53292       ; cat 3
                sta 53293       ; mouse 3
                lda #COLOR_YELLOW
                sta 53291       ; cheese
                lda #44;#108        
                sta 53277       ; sprite expand x
                sta 53271       ; sprite expand y

                lda #224
                sta SPRITE_X_MSB
                rts

setup_irq       sei
                lda #$7f
                ldx #$01
                sta $dc0d    ; Turn off CIA 1 interrupts
                sta $dd0d    ; Turn off CIA 2 interrupts
                stx $d01a    ; raster interrupts on

                ldy     #100 ; line interrupt
                sty     $d012           ; in interrupt vector

                lda $d011    ; zorgt ervoor dat het bit 8 van Y-raster uit staat (dus < 255)
                and #$7f
                sta $d011


                lda #<irq
                sta $0314
                lda #>irq
                sta $0315
                asl $d019

                cli
                RTS

irq             sei
                inc $d020        

                ;jsr $c003

                        lda     #100
                        sta     $d012
                        lda     $d011
                        and     #$7f
                        sta     $d011
                        asl     $D019   ; set interrupt to ready
                        lda #0
                        jsr animate_sprite
                        lda #1
                        jsr animate_sprite
                        lda #2
                        jsr animate_sprite
                        lda #3
                        jsr animate_sprite
                        lda #4
                        jsr animate_sprite
                        lda #5
                        jsr animate_sprite
                        lda #6
                        jsr animate_sprite
                        dec $d000
                        inc $d002
                        dec $d004
                        inc $d006
                        
                cli
                dec $d020
                jmp $ea31


current_sprite_tmp      byte 0
; a= sprite number
animate_sprite  tax     ; x =current sprite number
                lda     current_animation,x
                tay     ; y = current animation nr
                ; check if length is reached
                lda     animation_frame_length,y
                cmp     current_animation_count,x
                bne     as1
        ; frame length reached, new frame
                lda     #0      ; reset counter
                sta     current_animation_count,x
                tya
                asl     ; A times 2 gives pointer location
                tay     ; so Y is now animation number times 2
                lda     animation_pointer,y
                sta     self_mod+1
                lda     animation_pointer+1,y
                sta     self_mod+2
                ldy     current_frame,x         ; y is frame number current animation
self_mod        lda     $f0f0,y
                sta     SPRITE_POINTER1,x
                inc     current_frame,x
                lda     current_animation,x     ; make y current animation
                tay                             
                lda     current_frame,x
                cmp     animation_frame_count,y
                bne     animation_done
                lda     #0
                sta     current_frame,x
animation_done   rts

as1             inc     current_animation_count,x
                lda     current_animation_count,x
                rts


current_animation       
        byte 2,3,0,1,4,5,6,5
current_animation_count
        byte 0,0,0,0,0,0,0,0
current_frame
        byte 0,0,0,0,0,0,0,0
animation_frame_count
        byte 14,14,23,23,4,10,2
animation_frame_length
        byte 6,6,3,3,12,9,4

animation_pointer
        byte <cat_animate_left
        byte >cat_animate_left
        byte <cat_animate_right
        byte >cat_animate_right
        byte <mouse_animate_left
        byte >mouse_animate_left
        byte <mouse_animate_right
        byte >mouse_animate_right
        byte <cheese_animate
        byte >cheese_animate
        byte <cat_sit
        byte >cat_sit
        byte <mouse_massage
        byte >mouse_massage


cat_animate_left
        byte 192,193,194,195,194,193,192,193,194,195,194,193,192,202 
cat_animate_right
        byte 196,197,198,199,198,197,196,197,198,199,198,197,196,203
mouse_animate_left
        byte 209,210,211,212,217,209,210,211,212,217,209,210,211,212,217,209,210,211,212,217,219,219,219
mouse_animate_right
        byte 213,214,215,216,218,213,214,215,216,218,213,214,215,216,218,213,214,215,216,217,220,220,220
cheese_animate
        byte 205, 206,207,208
cat_sit byte 200,200,201,202,201,202,201,202,201,202
mouse_massage   byte 221,222

*=$3000
;============================ SPRITE DATA CAT ==================
start_sprite_data 
        incbin "cat.bin"
        incbin "mouse.bin"

sidwiz  = *
        incbin "include/helsing001.bin"