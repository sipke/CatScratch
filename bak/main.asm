;-----------------------------------------------
;       CONSTANTS
;----------------------------------------------
JOY1                    =$DC01
JOY2                    =$DC00

KEYCODE_SPACE           = 60
KEYCODE_2               = 59
KEYCODE_3               = 8
KEYCODE_4               = 11
KEYCODE_i               = 33
KEYCODE_s               = 13
KEYCODE_c               = 20

COLOR_BLACK           = 0
COLOR_WHITE           = 1
COLOR_RED             = 2
COLOR_CYAN            = 3
COLOR_PURPLE          = 4
COLOR_GREEN           = 5
COLOR_BLUE            = 6
COLOR_YELLOW          = 7
COLOR_ORANGE          = 8
COLOR_BROWN           = 9
COLOR_LIGHT_RED       = 10
COLOR_DARK_GREY       = 11
COLOR_MED_GREY        = 12
COLOR_LIGHT_GREEN     = 13
COLOR_LIGHT_BLUE      = 14
COLOR_LIGHT_GREY      = 15

BORDER_COLOR            =$D020
BACKGR_COLOR_0          = $D021
BACKGR_COLOR_1          = $D022
BACKGR_COLOR_2          = $D023
BACKGR_COLOR_3          = $D024

SPRITE_MULTICOLOR_1     = $D025
SPRITE_MULTICOLOR_2     = $D026

VICSTART                = $0000

SPRITE_POINTER                 =VICSTART+2040
SPRITE_POINTER1                =VICSTART+2040
SPRITE_POINTER2                =VICSTART+2041
SPRITE_POINTER3                =VICSTART+2042
SPRITE_POINTER4                =VICSTART+2043
SPRITE_POINTER5                =VICSTART+2044
SPRITE_POINTER6                =VICSTART+2045
SPRITE_POINTER7                =VICSTART+2046
SPRITE_POINTER8                =VICSTART+2047

SPRITE_ENABLE   = $D015


MULTICOLOR1             = $D025
MULTICOLOR2             = $D026

SPRITE_X_MSB            = $D010

RANDOM                  = $D41B

MAX_Y_SPEED             = #0

SPRITE_X_2_SCREEN_X     = #20
SPRITE_Y_2_SCREEN_Y     = #36

;--------- SID----------------------
SID_BASE                = $D400
FILTER_REGISTER_LO      = $D415
FILTER_REGISTER_HI      = $D416                

SID_WIZ_START           = $9000
SID_WIZ_BLOCKS          = #20
MUSIC_INIT              = SID_WIZ_START
MUSIC_PLAY              = SID_WIZ_START+3
EFFECT_PLAY             = SID_WIZ_START+12

*= $0801

GenerateTo   "catscratch.prg"

;--------------------------------------------------
;       BASIC STARTER
;--------------------------------------------------

        BYTE $0C, $08  ; pointer to next line
        BYTE $0A, $00  ; line number (10)
        BYTE $9E       ; SYS token

        TEXT " 2062" ; SYS address in ASCII
        
        BYTE 0, 0, 0 ; end-of-program

        ;;bytes 0 * 55
;--------------------------------------------------
;       start program
;--------------------------------------------------
                lda #$37
                sta $01
                jsr setup_sprites
                jsr setup_screen
                jsr copy_sid_wiz
                jsr setup_music
                jsr setup_irq
                jsr copy_sprites
                
endless         jmp endless
setup_screen    ldx #0
@loop           lda #$20
                sta $0400,x
                sta $0400+250,x
                sta $0400+500,x
                sta $0400+750,x                
                inx
                cpx #250
                bne @loop
                ldx #39
                lda #180
                sta 1950

                sta 1840
                sta 1880
                sta 1842
                
                sta 1784
                sta 1824
                sta 1864
                sta 1904
                sta 1944

                sta 1877
                sta 1817
                sta 1857
                sta 1897
                sta 1977
                sta 1937

@loop2          sta 1984,x
                dex
                bpl @loop2
                rts

setup_sprites   ldy #160
                ldx #0
                lda #30
@loop1          sta $d000,x
                inx
                inx
                clc
                adc #50
                cpx #12
                bne @loop1
                
                lda #49
                sta $d000+12

                ldx #0
                lda #100
@loop2          sta $d001,x
                inx
                inx
                cpx #12
                bne @loop2
                lda #160-21
                sta $d001+4
                sta $d001+6
                sta $d001+10
                lda #160-18
                sta $d001+12

                ldx #0
                lda #192
@loop3          sta SPRITE_POINTER1,x
                inx
                clc     
                adc #3
                cpx #8
                bne @loop3

                lda #3
                sta SPRITE_ENABLE
                sta 53276       ; multicolor        
                lda #COLOR_BLACK
                sta SPRITE_MULTICOLOR_2
                lda #COLOR_MED_GREY
                sta SPRITE_MULTICOLOR_1
                lda #COLOR_WHITE        
                sta 53287       ; mouse 1
                sta 53288       ; mouse 2
                sta 53289       ; cat 1
                sta 53290       ; cat 2
                sta 53292       ; cat 3
                sta 53293       ; mouse 3
                lda #COLOR_YELLOW
                sta 53291       ; cheese
                lda #44;#108        
                sta 53277       ; sprite expand x
                sta 53271       ; sprite expand y

                lda #224
                sta SPRITE_X_MSB
                rts

setup_irq       sei
                lda #$7f
                ldx #$01
                sta $dc0d    ; Turn off CIA 1 interrupts
                sta $dd0d    ; Turn off CIA 2 interrupts
                stx $d01a    ; raster interrupts on

                ldy     #100 ; line interrupt
                sty     $d012           ; in interrupt vector

                lda $d011    ; zorgt ervoor dat het bit 8 van Y-raster uit staat (dus < 255)
                and #$7f
                sta $d011


                lda #<irq
                sta $0314
                lda #>irq
                sta $0315
                asl $d019

                cli
                RTS

irq             sei
                inc $d020        


                jsr MUSIC_PLAY

                jsr joystick_read
                
                ldx #0
@cycle          stx current_mouse
                jsr position_change
                jsr animate_sprite
                ldx current_mouse
                inx
                cpx mouses_in_level
                bne @cycle


                        
                        lda     #10
                        sta     $d012
                        lda     $d011
                        and     #$7f
                        sta     $d011
                        asl     $D019   ; set interrupt to ready
                cli
                dec $d020
                jmp $ea31

joy_left        byte 0
joy_right       byte 0
joy_up          byte 0
joy_down        byte 0
joy_button      byte 0
in_air          byte 1,0,0,0,0,0
walking         byte 0,0,0,0,0,0
mouses_in_level byte 2
current_mouse   byte 0
controlled_mouse byte 0

joystick_read   ldx controlled_mouse
                lda #0
                sta walking,x
                lda JOY2
                lsr
                bcs @j1
        ;up
                ldy controlled_mouse
                iny
                cpy mouses_in_level     
                bne @n
                ldy #0
@n              sty controlled_mouse
        ; don't check other directions
                ;lda #MOUSE_BLINK_ANIMATE_LEFT
                ;sta current_animation,y
                lda #0
                sta current_animation_count
                sta current_frame
                rts
@j1             lsr
                bcs @j2
@j2             lsr
d0e00                bcs @j3
                tay
                lda #1
                sta walking,x
                lda #$F0
                sta v_x,x
                tya
@j3             lsr
                bcs @j4
                tay
                lda #1
                sta walking,x
                lda #$10
                sta v_x,x
                tya
@j4             lsr
                bcs @j5
                lda joy_button
                bne @j6
                lda in_air,x
                bne @j6
                lda #$e8
                sta v_y,x
                lda #16
                sta joy_button
                lda #0
; sound effect X=Note, Y=Instrument, A=Length (frames)
                ldx #70
                ldy #1
                lda #20
                jsr EFFECT_PLAY
                rts
@j6             dec joy_button
                rts
@j5             lda #$00
                sta joy_button
                rts


width           byte 16,24
height          byte 16,21
half_width      byte 8, 12
half_height     byte 8,10

;speed values per mouse
v_x             byte 0,0,0,0,0,0
v_y             byte 0,0,0,0,0,0
;position per mouse
pos_x           byte 100,150,0,0,0,0
pos_y           byte 100,100,0,0,0,0

; temporary values
;low precision
v_xlo           byte 0
v_ylo           byte 0
tmp_lo          = $fc
tmp_hi          = $fd
screen_pos_lo   byte 0       
screen_pos_hi   byte 0

new_x           byte 0
new_y           byte 0
cell_x          byte 0
cell_y          byte 0

; cells
; hab
; g.c
; fed
ca              byte 0
cb              byte 0 
cc              byte 0
cd              byte 0 
ce              byte 0
cf              byte 0 
cg              byte 0
ch              byte 0 


jmp_table_pos_lo        byte <sit_0, <sit_e, <sit_0, <sit_a, <sit_c, <sit_d, <sit_0, <sit_b 
                        byte <sit_0, <sit_0, <sit_0, <sit_0, <sit_g, <sit_f, <sit_0, <sit_h

jmp_table_pos_hi        byte >sit_0, >sit_e, >sit_0, >sit_a, >sit_c, >sit_d, >sit_0, >sit_b 
                        byte >sit_0, >sit_0, >sit_0, >sit_0, >sit_g, >sit_f, >sit_0, >sit_h

compare_outcome byte 0


position_change
                ldx current_mouse
                ; to low resolution speed
                lda v_x,x
                bpl @s1
                ; negative value
                eor #$ff
                lsr
                lsr
                lsr
                eor #$ff
                jmp @s2

@s1             lsr
                lsr
                lsr
@s2             sta v_xlo

                lda v_y,x
                bpl @s3
                ; negative value
                eor #$ff
                lsr
                lsr
                lsr
                eor #$ff
                jmp @s4

@s3             lsr
                lsr
                lsr
@s4             sta v_ylo
          
             ;calc possible new pos
                lda pos_y,x
                clc
                adc v_ylo
                sta new_y

                lda pos_x,x
                clc
                adc v_xlo
                sta new_x
             
            ; calculate cell/char to upper left of sprite
                jsr calc_screen_pos

                ldx current_mouse

                lda #0
                sta compare_outcome
            ; compare x and new x
                lda pos_x,x
                cmp new_x
                beq @eq1
                rol compare_outcome 
                sec
                rol compare_outcome     ; 01 or 11 -> compare if x movement
                jmp @cy

@eq1            clc                     ; 00 -> compare if no x movement
                rol compare_outcome
                rol compare_outcome
        ; compare y and new y
@cy             lda pos_y,x
                cmp new_y
                beq @eq2
                rol compare_outcome 
                sec
                rol compare_outcome     ; 01 or 11 -> compare if y movement
                jmp @ce

@eq2            clc                     ; 00 ->compare if no y movement
                rol compare_outcome
                rol compare_outcome

        ; lower 4 bits compare outcome now can be used as an index for an on-gosub 
        ; there is some overhead, only 8 of 16 are meaningful

@ce             ldy compare_outcome


set_switch_1    lda jmp_table_pos_lo,y
                sta switch_1 + 1
                lda jmp_table_pos_hi,y
                sta switch_1 + 2
switch_1        jsr switch_1       
                
                
                txa
                asl
                tay     ;  current sprite times 2->y for indexed positioning
;               vx and vy should now be correctly calculated 
                clc
;               take virtual position, update with speed 
                lda v_xlo
                adc pos_x,x
                sta pos_x,x
;               add factor for screen cooordinates to sprite coordinates
                adc SPRITE_X_2_SCREEN_X
                sta $d000,y
;               if more than 255 then turn on bit for current sprite        
                lda SPRITE_X_MSB
                and bit_and,x   ; always turn off bit
                bcc @do_y
                ora bit_or,x    ; only turn on if > 255
@do_y           sta SPRITE_X_MSB
                clc
                lda v_ylo
                adc pos_y,x
                sta pos_y,x
                adc SPRITE_Y_2_SCREEN_Y
                sta $d001,y

                ldy #121
                lda (tmp_lo),y
                cmp #$20
                bne @onground
                ldy #122
                lda (tmp_lo),y
                cmp #$20
                bne @onground
                ldy #123
                lda (tmp_lo),y
                cmp #$20
                bne @onground
                ;not on ground- apply gravity
                lda v_y,x
                bmi @negative_speed
                clc
                adc #2
                cmp MAX_Y_SPEED
                bcs @no_max1
        inc $d020
                lda MAX_Y_SPEED
@no_max1        sta v_y,x
                lda #1
                sta in_air,x
                jmp @apply_friction
@negative_speed clc
                adc #2
                sta v_y,x
                lda #1
                sta in_air,x
                jmp @apply_friction
                ; apply friction on x speed
@onground       lda #0
                sta in_air,x
                sta v_y,x
@apply_friction lda v_x,x
                beq standing_still
                bmi moving_left
                ;moving right
                sec
                sbc #1
                sta v_x,x
                rts
moving_left     clc
                adc #1
                sta v_x,x
standing_still  rts



sit_0           rts

; .|.
; .o.
; ...
sit_a           ldy #81
                lda (tmp_lo),y
                cmp #$20
                bne @obstructed
                ldy #82
                lda (tmp_lo),y
                cmp #$20
                bne @obstructed
                ldy #83
                lda (tmp_lo),y
                cmp #$20
                bne @obstructed
                rts
        ; is obstructed in y direction, now set y speed to 0
        ; and set pos_y to align with bottom edge of obstructing cell
@obstructed     sta (tmp_lo),y
                lda cell_y
                asl
                asl
                asl
                clc
                adc #24
                sta pos_y,x
                lda #0
                sta v_y,x
                sta v_ylo
                rts
; ../
; .o.
; ...
sit_b           ldy #82
                lda (tmp_lo),y
                cmp #$20
                bne @no_top
                ldy #83
                lda (tmp_lo),y
                cmp #$20
                bne @no_top
                ldy #84
                lda (tmp_lo),y
                cmp #$20
                bne @no_top_right
                rts
@no_top         ;up blocked
                sta (tmp_lo),y
                lda cell_y
                asl
                asl
                asl
                clc
                adc #24
                sta pos_y,x
                lda #0
                sta v_y,x
                sta v_ylo
                ;checkright
                jmp sit_c

@no_top_right   lda cell_x
                asl
                asl
                asl
                clc
                adc #6
                sta pos_x,x
                lda #0
                sta v_x,x
                sta v_xlo
                rts

; ...
; .o-
; ...
sit_c           ldy #84
                lda (tmp_lo),y
                cmp #$20
                bne @obstructed
                rts
        ; is obstructed in x direction, now set x speed to 0
        ; and set pos_x to align with left edge of obstructing cell
@obstructed     lda cell_x
                asl
                asl
                asl
                clc
                adc #8
                sta pos_x,x
                lda #0
                sta v_x,x
                sta v_xlo
                rts
; ...
; .o.
; ..\
sit_d           ldy #122
                lda (tmp_lo),y
                cmp #$20
                bne @no_down
                ldy #123
                lda (tmp_lo),y
                cmp #$20
                bne @no_down
                ldy #124
                lda (tmp_lo),y
                cmp #$20
                bne @no_down_right
                rts
@no_down         ;down blocked
                lda cell_y
                asl
                asl
                asl
                clc
                adc #8
                sta pos_y,x
                lda #0
                sta v_y,x
                sta v_ylo
                ;checkright
                jmp sit_c

@no_down_right  lda cell_x
                asl
                asl
                asl
                clc
                adc #6
                sta pos_x,x
                lda #0
                sta v_x,x
                sta v_xlo
                rts
; ...
; .o.
; .|.
sit_e           ldy #121
                lda (tmp_lo),y
                cmp #$20
                bne @onground
                ldy #122
                lda (tmp_lo),y
                cmp #$20
                bne @onground
                ldy #123
                lda (tmp_lo),y
                cmp #$20
                bne @onground
                rts
        
@onground       lda cell_y
                asl
                asl
                asl
                clc
                adc #8
                sta pos_y,x
                lda #0
                sta v_y,x
                sta v_ylo

                rts
; ...
; .o.
; /..

sit_f           ldy #122
                lda (tmp_lo),y
                cmp #$20
                bne @no_down
                ldy #121                
                lda (tmp_lo),y
                cmp #$20
                bne @no_down
                ldy #120
                lda (tmp_lo),y
                cmp #$20
                bne @no_down_right
                rts
@no_down         ;down blocked
                lda cell_y
                asl
                asl
                asl
                clc
                adc #8
                sta pos_y,x
                lda #0
                sta v_y,x
                sta v_ylo
                ;checkleft
                jmp sit_g

@no_down_right  lda cell_x
                asl
                asl
                asl
                clc
                adc #$10
                sta pos_x,x
                lda #0
                sta v_x,x
                sta v_xlo
                rts
; ...
; -o.
; ...
sit_g           ldy #80
                lda (tmp_lo),y
                cmp #$20
                bne @obstructed
                rts
        ; is obstructed in x direction, now set x speed to 0
        ; and set pos_x to align with right edge of obstructing cell
@obstructed     lda cell_x
                asl
                asl
                asl
                clc
                adc #$0a
                sta pos_x,x
                lda #0
                lda #MOUSE_RIGHT_ANIMATE
                sta current_animation,x
                sta v_x,x
                sta v_xlo
                rts
; \..
; .o.
; ...
sit_h           ldy #82
                lda (tmp_lo),y
                cmp #$20
                bne @no_top
                ldy #81
                lda (tmp_lo),y
                cmp #$20
                bne @no_top
                ldy #80
                lda (tmp_lo),y
                cmp #$20
                bne @no_top_left
                rts
@no_top         ;up blocked
                lda cell_y
                asl
                asl
                asl
                clc
                adc #24
                sta pos_y,x
                lda #0
                sta v_y,x
                sta v_ylo
                ;checkleft
                jmp sit_g

@no_top_left    lda cell_x
                asl
                asl
                asl
                clc
                adc #$10
                sta pos_x,x
                lda #0
                sta v_x,x
                sta v_xlo
                rts

;=========================================================================
;               CALC SCREEN POS FROM SPRITE POS
                
calc_screen_pos 
                ; from x and y pixel to x y cell : divide by 8
                ; also we use upper left cell from sprite as base cell  
                ; from that cell we can reach all other cells with an index
                ; so cell y-1 and cell x-1
                lda new_x
                lsr
                lsr
                lsr
                tay
                dey     ;should not be able to be lower than 0!
                sty cell_x        
                lda new_y
                lsr
                lsr
                lsr
                tay
                dey     ;should not be able to be lower than 0!           
                sty cell_y
     
           ; $03 -> tmp_hi
                lda #$03
                sta tmp_hi
                lda #216
           ; 216 -> lo (256-40) and add 40 times y to that
@loop           clc
                adc #40
                bcc @loop2
                inc tmp_hi
@loop2          dey
                bne @loop
                clc     
                sta add_this+1
                lda cell_x
add_this        adc #$00
                bcc @loop
                inc tmp_hi
@loop           sta tmp_lo     
                
        ; because sprite position can be outside screen some boundary checks
        ; can be omitted if levels are made properly        
                lda #$7f
                cmp tmp_hi
                bcc @too_high
                beq @equal1
                jmp @checktoolow
@equal1         lda #$e8
                cmp tmp_lo
                bcc @too_high
                jmp @checktoolow        
@too_high       lda #$7f
                sta tmp_hi
                lda #$e8
                sta tmp_lo
@checktoolow    lda tmp_hi
                cmp #4
                bcc @too_low
                rts                
@too_low        lda #4
                sta tmp_hi
                lda #0
                sta tmp_lo
                rts

; ======================================================================
;                     ANIMATION

animate_sprite  ldx current_mouse 
                lda in_air,x
                beq @checkwalk
                lda v_x,x
                bmi @in_air_left
                lda v_y,x
                bpl @falling_right
                lda #218
                sta SPRITE_POINTER,x
                rts
@falling_right  lda #220
                sta SPRITE_POINTER,x
                rts
@in_air_left    lda v_y,x
                bpl @falling_left
                lda #217
                sta SPRITE_POINTER,x
                rts
@falling_left   lda #219
                sta SPRITE_POINTER,x
                rts
@checkwalk      lda walking,x
                bne @do_walk
                lda current_animation,x
                cmp #MOUSE_LEFT_ANIMATE
                beq @still_left
                lda current_mouse
                cmp controlled_mouse
                ;beq @doblinkright
                lda #213
                sta SPRITE_POINTER,x
                rts
@doblinkright   lda #MOUSE_BLINK_ANIMATE_RIGHT
                jmp @next
@still_left     lda current_mouse
                cmp controlled_mouse
                ;beq @doblinkleft
                lda #209
                sta SPRITE_POINTER,x
                rts
@doblinkleft    lda #MOUSE_BLINK_ANIMATE_LEFT
                jmp @next
@do_walk        lda v_x,x
                bmi @walk_left
                lda #MOUSE_RIGHT_ANIMATE
                jmp @next
@walk_left      lda #MOUSE_LEFT_ANIMATE
@next           sta     current_animation,x
do_animate      lda     current_animation,x

                tay     ; y = current animation nr
                ; check if length is reached
                lda     animation_frame_length,y
                cmp     current_animation_count,x
                bne     as1
        ; frame length reached, new frame
                lda     #0      ; reset counter
                sta     current_animation_count,x
                tya
                asl     ; A times 2 gives pointer location
                tay     ; so Y is now animation number times 2
                lda     animation_pointer,y
                sta     self_mod+1
                lda     animation_pointer+1,y
                sta     self_mod+2
                ldy     current_frame,x         ; y is frame number current animation
self_mod        lda     $f0f0,y
                sta     SPRITE_POINTER1,x
                inc     current_frame,x
                lda     current_animation,x     ; make y current animation
                tay                             
                lda     current_frame,x
                cmp     animation_frame_count,y
                bne     animation_done
                lda     #0
                sta     current_frame,x
animation_done   rts

as1             inc     current_animation_count,x
                ;lda     current_animation_count,x
                rts

CAT_LEFT_ANIMATE = 0
CAT_RIGHT_ANIMATE = 1
MOUSE_LEFT_ANIMATE = 2
MOUSE_RIGHT_ANIMATE = 3
CHEESE_ANIMATION = 4
CAT_SIT_ANIMATE = 5
MOUSE_MASSAGE_ANIMATE = 6
MOUSE_BLINK_ANIMATE_RIGHT = 7
MOUSE_BLINK_ANIMATE_LEFT = 8


current_animation       
        byte 2,3,0,1,4,5,6,5
current_animation_count
        byte 0,0,0,0,0,0,0,0
current_frame
        byte 0,0,0,0,0,0,0,0
animation_frame_count
        byte 14,14,20,20,4,10,2,2,2
animation_frame_length
        byte 6,6,3,3,12,9,4,12,12

animation_pointer
        byte <cat_animate_left
        byte >cat_animate_left
        byte <cat_animate_right
        byte >cat_animate_right
        byte <mouse_animate_left
        byte >mouse_animate_left
        byte <mouse_animate_right
        byte >mouse_animate_right
        byte <cheese_animate
        byte >cheese_animate
        byte <cat_sit
        byte >cat_sit
        byte <mouse_massage
        byte >mouse_massage
        byte <mouse_blink_right
        byte >mouse_blink_right
        byte <mouse_blink_left
        byte >mouse_blink_left


cat_animate_left
        byte 192,193,194,195,194,193,192,193,194,195,194,193,192,202,0 
cat_animate_right
        byte 196,197,198,199,198,197,196,197,198,199,198,197,196,203,0
mouse_animate_left
        byte 209,210,211,212,217,209,210,211,212,217,209,210,211,212,217,209,210,211,212,217,0
mouse_animate_right
        byte 213,214,215,216,218,213,214,215,216,218,213,214,215,216,218,213,214,215,216,218,0
cheese_animate
        byte 205, 206,207,208,0
cat_sit byte 200,200,201,202,201,202,201,202,201,202,0
mouse_massage   byte 221,222,0
mouse_blink_right   byte 214,215,0
mouse_blink_left    byte 210,211,0


;  subtune must be set in accumulator before calling it.
setup_music     lda #1
                jsr MUSIC_INIT
                ;lda #1
                ;jsr MUSIC_INIT
                rts

;copy sidd wizard
copy_sid_wiz
        ldy #SID_WIZ_BLOCKS
@loop0  ldx #0
@loop   lda sidwiz,x
        sta SID_WIZ_START,x
        inx        
        bne @loop
        inc @loop+2
        inc @loop+5
        dey
        bne @loop0

        rts


copy_sprites_error    ldx #255
@loop           lda     start_sprite_data,x
                sta     $3000,x
                lda     start_sprite_data+256,x
                sta     $3000+256,x
                lda     start_sprite_data+512,x
                sta     $3000+512,x
                lda     start_sprite_data+768,x
                sta     $3000+768,x
                lda     start_sprite_data+1024,x
                sta     $3000+1024,x
                lda     start_sprite_data+1280,x
                sta     $3000+1280,x
                lda     start_sprite_data+1536,x
                sta     $3000+1536,x
                lda     start_sprite_data+1792,x
                sta     $3000+1792,x
                lda     start_sprite_data+2048,x
                sta     $3000+2048,x
                lda     start_sprite_data+2304,x
                sta     $3000+2304,x
                lda     start_sprite_data+2560,x
                sta     $3000+2560,x
                lda     start_sprite_data+2816,x
                sta     $3000+2816,x
                lda     start_sprite_data+3072,x
                sta     $3000+3072,x
                lda     start_sprite_data+3328,x
                sta     $3000+3328,x
                lda     start_sprite_data+3584,x
                sta     $3000+3584,x
                dex        
                cpx #255
                bne @loop
                rts
copy_sprites
                lda #<start_sprite_data
                sta cs2+1
                lda #>start_sprite_data
                sta cs2+2
                lda #$00
                sta cs3+1
                lda #$30
                sta cs3+2
                ldy #16
cs1             ldx #0
cs2             lda $FFFF,x
cs3             sta $3000,x
                inx
                bne cs2
                inc cs2+2
                inc cs3+2
                dey
                ;cpy #16
                bne cs1
                rts
                        

bit_or          byte %00000001,%00000010,%00000100,%00001000
                byte %00010000,%00100000,%01000000,%10000000
bit_and         byte %11111110,%11111101,%11111011,%11110111
                byte %11101111,%11011111,%10111111,%01111111

*=$3000
;============================ SPRITE DATA CAT ==================; 
 BYTE $00,$00,$00
 BYTE $00,$00,$00
 BYTE $00,$00,$0F
 BYTE $00,$00,$3F
 BYTE $00,$00,$FF
 BYTE $00,$03,$FF
 BYTE $00,$07,$FF
 BYTE $00,$1F,$FF
 BYTE $00,$3F,$FF
 BYTE $00,$7F,$FF
 BYTE $00,$FF,$FF
 BYTE $01,$FF,$FF
 BYTE $03,$FF,$FF
 BYTE $07,$FF,$FF
 BYTE $0F,$FF,$FF
 BYTE $0F,$FF,$FF
 BYTE $1F,$FF,$FF
 BYTE $1F,$FF,$FF
 BYTE $3F,$FF,$FF
 BYTE $3F,$FF,$FF
 BYTE $3F,$FF,$FF
 BYTE $00

; 
 BYTE $00,$00,$00
 BYTE $00,$00,$00
 BYTE $F0,$00,$00
 BYTE $FC,$00,$00
 BYTE $FF,$00,$00
 BYTE $FF,$C0,$00
 BYTE $FF,$E0,$00
 BYTE $FF,$F8,$00
 BYTE $FF,$FC,$00
 BYTE $FF,$FE,$00
 BYTE $FF,$FF,$00
 BYTE $FF,$FF,$80
 BYTE $FF,$FF,$C0
 BYTE $FF,$FF,$E0
 BYTE $FF,$FF,$F0
 BYTE $FF,$FF,$F0
 BYTE $FF,$FF,$F8
 BYTE $FF,$FF,$F8
 BYTE $FF,$FF,$FC
 BYTE $FF,$FF,$FC
 BYTE $FF,$FF,$FC
 BYTE $00

; 
 BYTE $FF,$FF,$FC
 BYTE $FF,$FF,$FC
 BYTE $FF,$FF,$FC
 BYTE $FF,$FF,$F8
 BYTE $FF,$FF,$F8
 BYTE $FF,$FF,$F0
 BYTE $FF,$FF,$F0
 BYTE $FF,$FF,$E0
 BYTE $FF,$FF,$C0
 BYTE $FF,$FF,$80
 BYTE $FF,$FF,$00
 BYTE $FF,$FE,$00
 BYTE $FF,$FC,$00
 BYTE $FF,$F8,$00
 BYTE $FF,$E0,$00
 BYTE $FF,$C0,$00
 BYTE $FF,$00,$00
 BYTE $FC,$00,$00
 BYTE $F0,$00,$00
 BYTE $00,$00,$00
 BYTE $00,$00,$00
 BYTE $00

; 
 BYTE $3F,$FF,$FF
 BYTE $3F,$FF,$FF
 BYTE $3F,$FF,$FF
 BYTE $1F,$FF,$FF
 BYTE $1F,$FF,$FF
 BYTE $0F,$FF,$FF
 BYTE $0F,$FF,$FF
 BYTE $07,$FF,$FF
 BYTE $03,$FF,$FF
 BYTE $01,$FF,$FF
 BYTE $00,$FF,$FF
 BYTE $00,$7F,$FF
 BYTE $00,$3F,$FF
 BYTE $00,$1F,$FF
 BYTE $00,$07,$FF
 BYTE $00,$03,$FF
 BYTE $00,$00,$FF
 BYTE $00,$00,$3F
 BYTE $00,$00,$0F
 BYTE $00,$00,$00
 BYTE $00,$00,$00
 BYTE $00

; 
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $00

; slit_eye_top
 BYTE $00,$00,$00
 BYTE $00,$18,$00
 BYTE $00,$3C,$00
 BYTE $00,$3C,$00
 BYTE $00,$7C,$00
 BYTE $00,$7C,$00
 BYTE $00,$7E,$00
 BYTE $00,$FF,$00
 BYTE $00,$FF,$00
 BYTE $00,$FF,$00
 BYTE $01,$FF,$80
 BYTE $01,$FF,$80
 BYTE $01,$FF,$C0
 BYTE $03,$FF,$C0
 BYTE $03,$FF,$C0
 BYTE $03,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$F0
 BYTE $00

; slit_eye_top_middle
 BYTE $07,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $0F,$FF,$E0
 BYTE $0F,$FF,$F0
 BYTE $0F,$FF,$F0
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $00

; slit_eye_bottom
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $03,$FF,$E0
 BYTE $03,$FF,$C0
 BYTE $03,$FF,$C0
 BYTE $01,$FF,$C0
 BYTE $01,$FF,$80
 BYTE $01,$FF,$80
 BYTE $00,$FF,$00
 BYTE $00,$FF,$00
 BYTE $00,$FF,$00
 BYTE $00,$7E,$00
 BYTE $00,$7C,$00
 BYTE $00,$7C,$00
 BYTE $00,$3C,$00
 BYTE $00,$3C,$00
 BYTE $00,$18,$00
 BYTE $00,$00,$00
 BYTE $00

; slit_eye_bottom_middle
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $3F,$FF,$FE
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $0F,$FF,$F0
 BYTE $0F,$FF,$F0
 BYTE $0F,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $00

; slit_eye_top_glow
 BYTE $00,$18,$00
 BYTE $00,$3E,$00
 BYTE $00,$7E,$00
 BYTE $00,$7E,$00
 BYTE $00,$FE,$00
 BYTE $00,$FF,$00
 BYTE $00,$FF,$00
 BYTE $01,$FF,$80
 BYTE $01,$FF,$80
 BYTE $01,$FF,$C0
 BYTE $03,$FF,$C0
 BYTE $03,$FF,$C0
 BYTE $03,$FF,$E0
 BYTE $07,$FF,$E0
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$F0
 BYTE $0F,$FF,$F0
 BYTE $0F,$FF,$F8
 BYTE $0F,$FF,$F8
 BYTE $0F,$FF,$F8
 BYTE $0F,$FF,$F8
 BYTE $00

; slit_eye_top_middle_glow
 BYTE $0F,$FF,$F0
 BYTE $0F,$FF,$F0
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FF
 BYTE $7F,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $00

; slit_eye_bottom_glow
 BYTE $0F,$FF,$FC
 BYTE $0F,$FF,$F8
 BYTE $0F,$FF,$F8
 BYTE $0F,$FF,$F8
 BYTE $0F,$FF,$F0
 BYTE $0F,$FF,$F0
 BYTE $07,$FF,$F0
 BYTE $07,$FF,$E0
 BYTE $03,$FF,$E0
 BYTE $03,$FF,$E0
 BYTE $03,$FF,$C0
 BYTE $03,$FF,$C0
 BYTE $01,$FF,$80
 BYTE $01,$FF,$80
 BYTE $01,$FF,$80
 BYTE $00,$FE,$00
 BYTE $00,$FE,$00
 BYTE $00,$FE,$00
 BYTE $00,$7E,$00
 BYTE $00,$7C,$00
 BYTE $00,$18,$00
 BYTE $00

; 
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $FF,$FF,$FF
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $7F,$FF,$FE
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$FC
 BYTE $3F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F8
 BYTE $1F,$FF,$F0
 BYTE $1F,$FF,$F0
 BYTE $0F,$FF,$F0
 BYTE $00


start_sprite_data 
        incbin "cat.bin"
        incbin "mouse.bin"

sidwiz  = * 
        incbin "music/catscr004.bin"
